const express = require("express");
const bodyParser = require("body-parser");
const { Webhook, MessageBuilder } = require("discord-webhook-node");

const app = express();
const port = 3000;

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }));

// parse application/json
app.use(bodyParser.json());

app.post("/send", (request, response) => {
  const hook = new Webhook(
    "https://discord.com/api/webhooks/900163142355669022/kowKVZxQeQOx_lT458bXGLxCTDhm6d12YCgTDYgEcwH8pT0CVB1noIy_sMRwOT5ihKFJ"
  );
  let avatar =
    "https://pbs.twimg.com/profile_images/1026981625291190272/35O2KIRX_400x400.jpg";
  let username = "Bitbucket Pipeline";
  if (request.body.from === "aws") {
    avatar =
      "https://customcodefactory.com/wp-content/uploads/2019/12/aws-app-icon.jpg";
    username = "AWS CodePipeline";
  }
  let description = request.body.description
    ? request.body.description
    : "Alphabetarena";
  hook.setUsername(username);
  hook.setAvatar(avatar);
  const embed = new MessageBuilder()
    .setTitle("Deployment Success")
    .setAuthor(username, avatar)
    .addField("Commit", request.body.commit_message)
    .addField("Author", request.body.author)
    .setColor("#34d700")
    .setDescription(description)
    .setTimestamp();

  hook.send(embed);

  response.send("success");
});

app.listen(port, () => {
  console.log(`App listening at http://localhost:${port}`);
});
